(function(){
    f5.Router = function( options ) {
        this.routes = {};
        this.extend( options || {} );
        this.addListeners();
        // Trigger first routing
        this.onHashChange();
    };

    f5.Router.prototype.addListeners = function() {
        window.onhashchange = this.onHashChange.bind(this);
    };

    f5.Router.prototype.onHashChange = function() {
        // let's remove the #
        var hash = (location.hash || '#').substr(1);
        hash = hash || '';
        if ( this.routes && this.routes[hash] ) {
            var method = this.routes[hash];
            if ( method ) {
                this[method].call(this);
            }
        }
    };

    f5.Router.prototype.extend = function( obj ) {
        for( var j in obj ) {
            if ( obj.hasOwnProperty(j) ) {
                this[ j ] = obj[j];
            }
        }
    };

})();

